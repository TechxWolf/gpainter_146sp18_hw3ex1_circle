// Griffin Painter
// Spring 2018
// CSCI 146
// Homework 3, problem 1
public class Circle {
    
    /* FIELDS */
    private double radius;
    
    /* CONSTRUCTOR */
    // circle constructor, where we set the parameters ( which there are none
    // here) and set the value, which are defualt will be zero
    public Circle() {
        radius = 0;
    }// end constructor
    
    /* METHODS */
    // overloaded constructor circle, where the value is going to be set radius 
    // to a double, allowing more accuracy, and pie is a double, so it will make
    // the programming easier as well as give correct answers. The parameter
    // for this overloaded method is the radius
    public Circle(double radius) {
        this.radius = radius;
    }//end overloaded constructor

    // get method for radius
    public double getRadius() {
        return radius;
    }// end get method

    // set method for radius
    public void setRadius(double radius) {
        this.radius = radius;
    }//end set method
    
    // we are setting the method getCircumference using knnowledge we know
    // and our variable radius to complete the solution
    public double getCircumference() {
        return 2 * 3.14159 * radius;
    }// end getCirc. method
    
    // we are setting the method for solving the area of our circle, using data
    // we know and our other variables
    public double getArea() {
        return 3.14159 * radius * radius;
    }// end get Area method
    
    //solving for the diameter, which is simple, multiplying the radius given
    // by 2. 
    public double getDiameter() {
        return radius * 2;
    }// end getDiam method
}// end class Circle

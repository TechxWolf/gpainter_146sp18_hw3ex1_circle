// Griffin Painter
// Spring 2018
// CSCI 146
// Homework 3, problem 3
import java.util.Scanner;

// Start of Main, here we will be taking in the data from the circle
// and solving and outputting the data. Here we will also prompt the user to put
// in the information needed for circle, such as radius and area.
public class Main {

    // basic start of the main, using the classic java syntax for a list of
    // arguments, basically this is where we are gonna code what we stated above
    public static void main(String[] args) {
        System.out.println("Welcome to the Circle Calculator");
        System.out.println();
 
        Scanner sc = new Scanner(System.in);
        
        String choice = "y";
        
        while (choice.equals("y")) {
            // get input from user
            System.out.print("Enter one or more raddi:  ");
            String line = sc.nextLine();
            line = line.trim();
            String entries[] = line.split(" "); // this splits our users
                                                // inputted data into two radii
            //double radius = Double.parseDouble(sc.nextLine());
            
           Circle circles[] = new Circle[entries.length];// create a new array 
                                                         // circles that has the
                                                         // same length as array
                                                         // entries
                                                         
                                                         
            // I tried looking up parseDouble on API and couldnt find anything
            // useful so I will include my source for reference
            // https://stackoverflow.com/questions/10577610/what-is-the-difference-between-double-parsedoublestring-and-double-valueofstr
            for ( int i = 0; i < entries.length; i++)
            {
            
            circles[i] = new Circle(Double.parseDouble(entries[i]));
            
            
            }//end for loop parsedouble
            // Note: we use entries.length because its the base of circles
            // length, so it makes more sense to use the base just in case there 
            // is an error in our circles array method
            
             //Circle circle = new Circle(radius);
             
             // https://blogs.oracle.com/corejavatechtips/using-enhanced-for-loops-with-your-classes
             // for ( int i = 0; i < entries.length; i++) -- Backup
             // Note have to complete enhanced for loop
             
           for ( Circle c : circles )
            {
              String message = 
                "Radius:        " + c.getRadius() + "\n" +     
                "Area:          " + c.getArea() + "\n" +
                "Circumference: " + c.getCircumference() + "\n" +
                "Diameter:      " + c.getDiameter() + "\n";
            System.out.println(message);
                
            }// end print info for loop

                
         

            // create the Circle object
            //Circle circle = new Circle(radius);
            /*
            // format and display output
            String message = 
                "Area:          " + circle.getArea() + "\n" +
                "Circumference: " + circle.getCircumference() + "\n" +
                "Diameter:      " + circle.getDiameter() + "\n";
            System.out.println(message);
            */
            // see if the user wants to continue5
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        }
        
        System.out.println("Bye!");
        sc.close();
        
    }   
}